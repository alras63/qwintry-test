# Запуск
1. Создать клон проекта
```sh
git clone https://gitlab.com/alras63/qwintry-test.git
```

2. Запустить Docker на вашем устройстве (https://www.docker.com/)
3. `composer install`
4. Настроить `.env` на основе `.env.example`:
    1. `APP_URL` - адрес сервера
    2. `APP_PORT` - порт приложения
    3. `DB_CONNECTION`=`mysql`
    4. `DB_HOST`=`mysql`
    5. `DB_PORT`=`3306`
    6. `FORWARD_DB_PORT`=`3307`
    7. `DB_DATABASE`=`laravel`
    8. `DB_USERNAME`=`sail`
    9. `DB_PASSWORD`=`password`

5. Запустить команду ` ./vendor/bin/sail build --no-cache && ./vendor/bin/sail up` из директории проекта
5. Запустить команду `./vendor/bin/sail artisan migrate` из директории проекта

# Прогресс выполнения
Не все успел сделать на беке и на фронте, но, думаю, основные свои возможности показал. При желании и возможности могу на выходных доделать!
