<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TasksController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/auth/register', [AuthController::class, 'register']);
Route::post('/auth/login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/tasks/list', function (Request $request) {
        return (new TasksController())->list($request);
    });

    Route::post('/tasks/create', function (Request $request) {
        return (new TasksController())->create($request);
    });

    Route::delete('/tasks/{id}', function (int $id) {
        return (new TasksController())->delete($id);
    });

    Route::post('/tasks/{id}/edit-status', function (int $id, Request $request) {
        return (new TasksController())->editUrgentStatus($id, $request);
    });


});
