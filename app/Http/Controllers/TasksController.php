<?php

namespace App\Http\Controllers;

use App\Http\Resources\TaskResource;
use App\Models\LinkTasksToUser;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TasksController extends Controller
{
    public function list(Request $request)
    {
        return TaskResource::collection(Task::join('link_tasks_to_users', 'link_tasks_to_users.task_id', '=', 'tasks.id')->where('link_tasks_to_users.user_id', Auth::id())->get());
    }

    public function create(Request $request)
    {
        try {
            $validate= Validator::make($request->all(),
                [
                    'title' => 'required|max:255',
                    'description' => 'string',
                    'is_urgent' => 'boolean',
                ]);

            if($validate->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'Ошибка валидации',
                    'errors' => $validate->errors()
                ], 400);
            }

            DB::beginTransaction();

            $task = Task::create([
                'title' => $request->title,
                'description' => $request->description,
                'expires_at' => Carbon::make($request->expires_at)?->format('Y-m-d H:i:s') ?? NULL,
                'is_urgent' => $request->is_urgent
            ]);

            LinkTasksToUser::create([
                'user_id' => Auth::id(),
                'task_id' => $task->id,
            ]);

            DB::commit();

            return response()->json([
                'status' => true,
                'message' => 'Задача успешно создана',
                'data' => $task
            ], 200);

        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function delete(int $taskId)
    {
        try {
            if(Task::whereId($taskId)->count() !== 0) {
                Task::whereId($taskId)->first()->delete();

                return response()->json([
                    'status' => true,
                    'message' => 'Задача успешно удалена',
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Ошибка валидации',
                    'errors' => array('id' => 'Такой задачи нет')
                ], 400);
            }



        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function editUrgentStatus(int $taskId, Request $request)
    {
        try {
            $validate= Validator::make($request->all(),
                [
                    'is_urgent' => 'boolean',
                ]);

            if($validate->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'Ошибка валидации',
                    'errors' => $validate->errors()
                ], 400);
            }

            if(Task::whereId($taskId)->count() !== 0) {
                $task = Task::whereId($taskId)->first();
                $task->is_urgent = $request->is_urgent;
                $task->save();

                return response()->json([
                    'status' => true,
                    'message' => 'Задача успешно изменена',
                    'data' => $task
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Ошибка валидации',
                    'errors' => array('id' => 'Такой задачи нет')
                ], 400);
            }



        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}
